<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('Administrator')->to('users_manage');
        Bouncer::allow('Guest')->to('guest');
    }
}
