    <script src="{{ URL::asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>

    <!-- This is data table -->
    <script src="{{ URL::asset('theme/assets/node_modules/datatables/datatables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->

    <!-- Bootstrap tether core JavaScript -->
    <script src="{{ URL::asset('theme/assets/node_modules/popper/popper.min.js') }}"></script> 
    <script src="{{ URL::asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Slimscrollbar scrollbar JavaScript -->
    <script src="{{ URL::asset('theme/ecommerce/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>

    <!-- Wave Effects -->
    <script src="{{ URL::asset('theme/ecommerce/dist/js/waves.js') }}"></script>

    <!-- Menu sidebar -->
    <script src="{{ URL::asset('theme/ecommerce/dist/js/sidebarmenu.js') }}"></script>

    <!-- Sticky kit -->
    <script src="{{ URL::asset('theme/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ URL::asset('theme/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('theme/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Summer Note -->
    <script src="{{ URL::asset('theme/assets/node_modules/summernote/dist/summernote-bs4.min.js') }}"></script>

    <!-- Toast -->
    <script src="{{ URL::asset('theme/assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>

    <!-- Custom JavaScript -->
    <script src="{{ URL::asset('theme/ecommerce/dist/js/custom.js') }}"></script>

    <script src="{{ URL::asset('theme/ecommerce/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ URL::asset('theme/ecommerce/dist/js/jquery.multi-select.js') }}"></script>

    <!-- Lines commented to avoid issues and warnings
    <script src="{{ URL::asset('theme/ecommerce/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="http://itoc-tools.triara.mexico/itoc/resources/libraries/jstree/jstree.min.js" type="text/javascript"></script>
    -->

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/e9f94371e8.js" crossorigin="anonymous"></script>

    <script>

        @if(Session::has('notification'))
            // Toast messages
            $.toast({
                heading: '{{ Session::get('notification.header') }}',
                text: '{{ Session::get('notification.message') }}',
                textAlign: 'left',
                position: 'top-right',
                loader: true,
                loaderBg:'#ff6849',
                icon: '{{ Session::get('notification.type', 'info') }}',
                showHideTransition: 'fade',
                allowToastClose: true, // fade, slide or plain
                hideAfter: 1800,
                stack: 5
            });
        @endif

        $(function(){

            //Toggle fullscreen
            $(".fullscreen-btn").click(function(e) {
                e.preventDefault();
                var $this = $(this);
                $this.children('i')
                    .toggleClass('fa-expand')
                    .toggleClass('fa-compress');
                $(this).closest('.card').toggleClass('panel-fullscreen');
            });

            $('#connect').on('click', function(){
                host = $('#host').val();
                os = $('#os').val();
                user = $('#user').val();
                password = $('#password').val();
                $('#host_error').text('');
                $('#os_error').text('');
                $('#user_error').text('');
                $('#password_error').text('');
                $.ajax ({
                    data: {
                        host: host,
                        os: os,
                        user: user,
                        password: password
                    },
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    type: "POST",
                    url: "{{ url('devices-connect') }}",
                    success: function(data) {
                        //console.log(data);
                        window.open(data,'_blank');
                    },
                    error: function(data) {
                        if( data.status === 422 ) {
                            var errors = data.responseJSON.errors;
                            $.each(errors, function(key, value) {
                                $("#" + key + "_error").text(value[0]);
                            });
                        }
                    }
                });
            });

            $('#save').on('click', function(){
                host = $('#host').val();
                os = $('#os').val();
                user = $('#user').val();
                password = $('#password').val();
                description = $('#description').val();
                $('#host_error').text('');
                $('#os_error').text('');
                $('#user_error').text('');
                $('#password_error').text('');
                $.ajax ({
                    data: {
                        host: host,
                        os: os,
                        user: user,
                        password: password,
                        description: description
                    },
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    type: "POST",
                    url: "{{ url('devices-save') }}",
                    success: function(data) {
                        console.log(data);
                    },
                    error: function(data) {
                        if( data.status === 422 ) {
                            var errors = data.responseJSON.errors;
                            $.each(errors, function(key, value) {
                                $("#" + key + "_error").text(value[0]);
                            });
                        } else{
                            console.log(data);
                        }
                    }
                });
            });

            $("img").addClass("img-responsive");

            // NOTE: Objeto con la configuración por default para cada DataTable

            /* DOM. Cada elemento en Data Tables tiene una letra asociada. Cada una de estas
             * letras es utilizada para indicar donde debe aprecer cada elemento en el DOM.
             * l - length changing input control
             * f - filtering input
             * t - The table
             * i - Table information summary 
             * p - pagination control
             * r - processing display element
             * Cada letra puede ser utlizada multiples veces (con exepción de la tabla en si).
             */

            window.DATATABLE = {
                scrollY_45: '45vh',
                scrollY_50: '50vh',
                scrollY_60: '60vh',
                scrollCollapse: true,
                bDestroy: true,
                domButtons: '<B><"clear"><lf>rtip',
                oLanguage: {
                    sProcessing: 'Procesando...',
                    sLengthMenu: 'Mostrar _MENU_',
                    sZeroRecords: 'No se encontraron resultados',
                    sSearch: 'Buscar',
                    sEmptyTable: 'No hay datos disponibles',
                    sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_',
                    sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0',
                    sInfoFiltered: '(Filtrado de un total de _MAX_)',
                    sInfoThousands: ',',
                    sLoadingRecords: 'Cargando...',
                    oPaginate: {
                        sFirst: 'Primero',
                        sLast: 'Ultimo',
                        sNext: 'Siguiente',
                        sPrevious: 'Anterior'
                    }
                },
                buttons: [
                    {
                        extend: 'excel',
                        className: 'btn btn-light btn-circle',
                        text: '<i class="far fa-file-excel fa-lg"></i>',
                        init: function(api, node, config) {
                           $(node).removeClass('dt-button');
                        },
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-light btn-circle',
                        text: '<i class="far fa-file-pdf fa-lg"></i>',
                        init: function(api, node, config) {
                           $(node).removeClass('dt-button');
                        },
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    },
                    {
                        extend: 'print',
                        className: 'btn btn-light btn-circle',
                        //text: '<i class="icon-printer"></i>',
                        text: '<i class="fas fa-print fa-lg"></i>',
                        init: function(api, node, config) {
                           $(node).removeClass('dt-button');
                        },
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ],
                aLengthMenu: [
                    [10,15,25, 50, 100, -1],
                    [10,15,25, 50, 100, 'Todo']
                ]
            };
            Object.freeze(DATATABLE);

            $('#users-table').DataTable({
                dom: DATATABLE.domButtons,
                bDestroy: DATATABLE.bDestroy,
                oLanguage: DATATABLE.oLanguage,
                buttons: DATATABLE.buttons,
                aLengthMenu: DATATABLE.aLengthMenu,
                ajax: '{{ url('users-list') }}',
                order: [[0, 'asc' ]],
                columns: [
                    {
                        title: 'ID',
                        data: 'id',
                        orderable: true
                    },
                    { title: 'Nombre',
                        data: function(data){
                            url = '<a href="{{ URL::route('users.show',':id') }}">' + data.name + '</a>';
                            url = url.replace(':id', data.id);
                            return url;
                        }
                    },
                    { title: 'Email', data: 'email'},
                    { title: 'Creado', data: 'created_at'}
                ]
            });

            $('#events-table').DataTable();

            $('#roles-table').DataTable({
                dom: DATATABLE.domButtons,
                bDestroy: DATATABLE.bDestroy,
                oLanguage: DATATABLE.oLanguage,
                buttons: DATATABLE.buttons,
                aLengthMenu: DATATABLE.aLengthMenu,
                processing: true,
                serverSide: true,
                ajax: '{{ url('roles-list') }}',
                order: [[0, 'asc' ]],
                columns: [
                    { title: 'ID', data: 'id'},
                    { title: 'Nombre',
                        data: function(data){
                            url = '<a href="{{ URL::route('roles.show',':id') }}">' + data.name + '</a>';
                            url = url.replace(':id', data.id);
                            return url;
                        }
                    },
                    { title: 'Titulo', data: 'title'},
                    { title: 'Creado', data: 'created_at'}
                ]
            });

            $('#abilities-table').DataTable({
                dom: DATATABLE.domButtons,
                bDestroy: DATATABLE.bDestroy,
                oLanguage: DATATABLE.oLanguage,
                buttons: DATATABLE.buttons,
                aLengthMenu: DATATABLE.aLengthMenu,
                processing: true,
                serverSide: true,
                ajax: '{{ url('abilities-list') }}',
                order: [[0, 'asc' ]],
                columns: [
                    { title: 'ID', data: 'id'},
                    { title: 'Nombre',
                        data: function(data){
                            url = '<a href="{{ URL::route('abilities.show',':id') }}">' + data.name + '</a>';
                            url = url.replace(':id', data.id);
                            return url;
                        }
                    },
                    { title: 'Titulo', data: 'title'},
                    { title: 'Creado', data: 'created_at'}
                ]
            });

            // Summer Note
            $('#content').summernote({
                height: 500,
                lang: 'es-ES',
                dialogsInBody: true,
                dialogsFade: false,
                //placeholder: '...Y aquí algo muy interesante y útil...',
                //*
                callbacks: {
                    onInit: function() {
                        var noteBtn = '<button id="makeTag" type="button" class="btn btn-primary btn-sm btn-small" title="Tag" data-event="something" tabindex="-1"><i class="fa fa-tag"></i></button>';
                        var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
                        $(fileGroup).appendTo($('.note-toolbar'));
                        $('#makeTag').tooltip({container: 'body', placement: 'bottom'});
                        $('#makeTag').click(function(event) {
                            var highlight = window.getSelection();
                            spn = document.createElement('span');
                            range = highlight.getRangeAt(0);
                            spn.innerHTML = highlight;
                            spn.className = 'label label-primary';
                            range.deleteContents();
                            range.insertNode(spn);
                        });
                    },
                    onPaste: function(image){
                        // TODO. Soporte para copy paste, cambiar Base64.
                    },
                    onImageUpload: function(image){
                        imageUpload(image[0]);
                    },
                },
                //*/
                toolbar: [
                    ['style', ['style']],
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    //['insert', ['picture', 'link', 'video']],
                    ['insert', ['picture', 'link','hr', 'video']],
                    ['table', ['table']],
                    ['help', ['help']],
                    ['fullscreen', ['fullscreen']],
                    ['codeview',['codeview']]
                ]
            }).on( 'change keyup keydown paste cut', 'textarea', function (){
                $(this).height(0).height(this.scrollHeight);
            }).find( 'textarea' ).change();

          // Switchery
          var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function () {
                new Switchery($(this)[0], $(this).data());
            });
            /* For select 2 Lines commented to avoid issues and warnings
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            changeTemplate();
            */

        });

        function imageUpload(image) {
            var data = new FormData();
            data.append("image",image);
            $.ajax ({
                data: data,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                type: "POST",
                url: "{{ url('document-image-upload') }}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    $('.summernote').summernote("insertImage", url);
                },
                error: function(data) {
                }
            });
        }

        // --- Del archivo scripts_create.blade.php se agrego el evento a onchange
        function changeTemplate() {
            var data = "";
            var text= $('#template_id').val();
            if(text) {
                data = jQuery.parseJSON(text)['description'];
            }
            $("#template_description").html(data);
        }
        
    </script>