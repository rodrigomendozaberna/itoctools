    <!-- Nestable -->
    <script src="{{ URL::asset('theme/assets/node_modules/nestable/jquery.nestable.js') }}"></script>
    <!-- Select2 custom -->
    <script src="{{ URL::asset('theme/assets/node_modules/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <!-- Select2 custom -->
    <script src="{{ URL::asset('theme/assets/node_modules/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js') }}" type="text/javascript"></script>
    <!-- multi-select css -->
    <script src="{{ URL::asset('theme/assets/node_modules/multiselect/js/jquery.multi-select.js') }}" type="text/javascript"></script>

    <script>
        $(function() {

            /**
             * 
             * Variables globales  
             */
            const property_var = [{
                value: 1,
                text: 'Category'
            }, {
                value: 2,
                text: 'Description'
            }, {
                value: 3,
                text: 'Lifecycle State'
            }, {
                value: 4,
                text: 'Priority'
            }, {
                value: 5,
                text: 'Severity'
            }, {
                value: 6,
                text: 'Subcategory'
            }, {
                value: 7,
                text: 'Title'
            }, {
                value: 8,
                text: 'Type'
            }, {
                value: 9,
                text: 'Application'
            }, {
                value: 10,
                text: 'CI Type'
            }, {
                value: 11,
                text: 'Key'
            }, {
                value: 12,
                text: 'Object'
            }, {
                value: 13,
                text: 'Original Data'
            }, {
                value: 14,
                text: 'Solution'
            }, {
                value: 15,
                text: 'Custom Attribute'
            }];

            const exp_logic_var = [{
                value: 1,
                text: 'equals'
            }, {
                value: 2,
                text: 'not equals'
            }, {
                value: 3,
                text: 'contains'
            }, {
                value: 4,
                text: 'not contains'
            }, {
                value: 5,
                text: 'matches'
            }, {
                value: 6,
                text: 'not matches'
            }, {
                value: 7,
                text: 'is one of'
            }];

            const operadores_var = [{
                value: 1,
                text: 'AND'
            }, {
                value: 2,
                text: 'OR'
            }, {
                value: 3,
                text: 'NOT'
            }];


            /* ------------------------------- Nestable ------------------------------- */
            var updateOutput = function(e) {
                var list = e.length ? e : $(e.target),
                    output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);
            updateOutput($('#nestable').data('output', $('#nestable-output')));

            /* ------------------------------- Config title and tool filter ------------------------------- */
            $('#tittle-filter').editable({
                validate: function(value) {
                    if ($.trim(value) == '') return 'Este campo es requerido';
                },
                mode: 'inline'
            });
            $('#tool-origin').editable({
                prepend: "not selected",
                mode: 'inline',
                source: [{
                    value: 1,
                    text: 'Youbora'
                }, {
                    value: 2,
                    text: 'AWS'
                }, {
                    value: 3,
                    text: 'New Relic'
                }],
                display: function(value, sourceData) {
                    var colors = {
                            "": "#98a6ad",
                            1: "#5fbeaa",
                            2: "#5d9cec"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });

                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            $('#tool-target').editable({
                prepend: "not selected",
                mode: 'inline',
                source: [{
                    value: 1,
                    text: 'OMI'
                }, {
                    value: 2,
                    text: 'OBM'
                }, {
                    value: 3,
                    text: 'New Relic'
                }],
                display: function(value, sourceData) {
                    var colors = {
                            "": "#98a6ad",
                            1: "#5fbeaa",
                            2: "#5d9cec"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });

                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });

            /* ------------------------------- Config Properties Start ------------------------------- */
            /* ------- Select for properties events -----------*/
            $('#properties').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 2 -----------*/
            $('#properties2').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression2').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter2').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators2').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 3 -----------*/
            $('#properties3').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression3').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter3').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators3').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 4 -----------*/
            $('#properties4').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression4').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter4').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators4').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 5 -----------*/
            $('#properties5').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression5').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter5').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators5').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 6 -----------*/
            $('#properties6').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression6').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter6').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators6').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 7 -----------*/
            $('#properties7').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression7').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter7').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators7').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 8 -----------*/
            $('#properties8').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression8').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter8').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators8').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 9 -----------*/
            $('#properties9').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression9').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter9').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators9').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 10 -----------*/
            $('#properties10').editable({
                prepend: "-Properties-",
                mode: 'inline',
                source: property_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#00ff90"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Select para la Expresion logica */
            $('#expression10').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter10').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators10').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 11 -----------*/
            /* Custom Param */
            $('#inline-parameter1101').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Expresion logica */
            $('#expression11').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter1102').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators11').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 12 -----------*/
            /* Custom Param */
            $('#inline-parameter1201').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Expresion logica */
            $('#expression12').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter1202').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators12').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 13 -----------*/
            /* Custom Param */
            $('#inline-parameter1301').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Expresion logica */
            $('#expression13').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter1302').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators13').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------- Select para las propiedades del evento 14 -----------*/
            /* Custom Param */
            $('#inline-parameter1401').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Expresion logica */
            $('#expression14').editable({
                prepend: "-Expression-",
                mode: 'inline',
                source: exp_logic_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#1124f2"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* Insert Param */
            $('#inline-parameter1402').editable({
                type: 'text',
                pk: 1,
                name: 'parameter',
                title: 'Enter Parameter',
                mode: 'inline'
            });
            /* Select para la Operador logica */
            $('#operators14').editable({
                prepend: "-Operators-",
                mode: 'inline',
                source: operadores_var,
                display: function(value, sourceData) {
                    var colors = {
                            "": "#fa0c0c"
                        },
                        elem = $.grep(sourceData, function(o) {
                            return o.value == value;
                        });
                    if (elem.length) {
                        $(this).text(elem[0].text).css("color", colors[value]);
                    } else {
                        $(this).empty();
                    }
                }
            });
            /* ------------------------------- Config Properties Ends ------------------------------- */

            /*  Add Condition Button */
            $('#add-row').on('click', function() {
                let node2 = document.getElementById("2");
                let node3 = document.getElementById("3");
                let node4 = document.getElementById("4");
                let node5 = document.getElementById("5");
                let node6 = document.getElementById("6");
                let node7 = document.getElementById("7");
                let node8 = document.getElementById("8");
                let node9 = document.getElementById("9");
                let node10 = document.getElementById("10");
                let oper_node1 = document.getElementById("11");
                let oper_node2 = document.getElementById("22");
                let oper_node3 = document.getElementById("33");
                let oper_node4 = document.getElementById("44");
                let oper_node5 = document.getElementById("55");
                let oper_node6 = document.getElementById("66");
                let oper_node7 = document.getElementById("77");
                let oper_node8 = document.getElementById("88");
                let oper_node9 = document.getElementById("99");
                if (node2.style.display == 'none') {
                    oper_node1.style.display = "block";
                    node2.style.display = "block";
                } else if (node3.style.display == 'none') {
                    oper_node2.style.display = "block";
                    node3.style.display = "block";
                } else if (node4.style.display == 'none') {
                    oper_node3.style.display = "block";
                    node4.style.display = "block";
                } else if (node5.style.display == 'none') {
                    oper_node4.style.display = "block";
                    node5.style.display = "block";
                } else if (node6.style.display == 'none') {
                    oper_node5.style.display = "block";
                    node6.style.display = "block";
                } else if (node7.style.display == 'none') {
                    oper_node6.style.display = "block";
                    node7.style.display = "block";
                } else if (node8.style.display == 'none') {
                    oper_node7.style.display = "block";
                    node8.style.display = "block";
                } else if (node9.style.display == 'none') {
                    oper_node8.style.display = "block";
                    node9.style.display = "block";
                } else if (node10.style.display == 'none') {
                    oper_node9.style.display = "block";
                    node10.style.display = "block";
                }
            });
            /*  Delete Condition Button */
            $('#delete-row').on('click', function() {
                let node2 = document.getElementById("2");
                let node3 = document.getElementById("3");
                let node4 = document.getElementById("4");
                let node5 = document.getElementById("5");
                let node6 = document.getElementById("6");
                let node7 = document.getElementById("7");
                let node8 = document.getElementById("8");
                let node9 = document.getElementById("9");
                let node10 = document.getElementById("10");
                let oper_node1 = document.getElementById("11");
                let oper_node2 = document.getElementById("22");
                let oper_node3 = document.getElementById("33");
                let oper_node4 = document.getElementById("44");
                let oper_node5 = document.getElementById("55");
                let oper_node6 = document.getElementById("66");
                let oper_node7 = document.getElementById("77");
                let oper_node8 = document.getElementById("88");
                let oper_node9 = document.getElementById("99");
                if (node10.style.display == 'block') {
                    oper_node9.style.display = "none";
                    node10.style.display = "none";
                } else if (node9.style.display == 'block') {
                    oper_node8.style.display = "none";
                    node9.style.display = "none";
                } else if (node8.style.display == 'block') {
                    oper_node7.style.display = "none";
                    node8.style.display = "none";
                } else if (node7.style.display == 'block') {
                    oper_node6.style.display = "none";
                    node7.style.display = "none";
                } else if (node6.style.display == 'block') {
                    oper_node5.style.display = "none";
                    node6.style.display = "none";
                } else if (node5.style.display == 'block') {
                    oper_node4.style.display = "none";
                    node5.style.display = "none";
                } else if (node4.style.display == 'block') {
                    oper_node3.style.display = "none";
                    node4.style.display = "none";
                } else if (node3.style.display == 'block') {
                    oper_node2.style.display = "none";
                    node3.style.display = "none";
                } else if (node2.style.display == 'block') {
                    oper_node1.style.display = "none";
                    node2.style.display = "none";
                }
            });
            /*  Add Custom Button */
            $('#add-custom').on('click', function() {
                let node110 = document.getElementById("110");
                let node120 = document.getElementById("120");
                let node130 = document.getElementById("130");
                let node140 = document.getElementById("140");
                let oper_node10 = document.getElementById("1010");
                let oper_node1010 = document.getElementById("10100");
                let oper_node1020 = document.getElementById("10200");
                let oper_node1030 = document.getElementById("10300");
                if (node110.style.display == 'none') {
                    node110.style.display = "block";
                    oper_node10.style.display = "block";
                } else if (node120.style.display == 'none') {
                    node120.style.display = "block";
                    oper_node1010.style.display = "block";
                } else if (node130.style.display == 'none') {
                    node130.style.display = "block";
                    oper_node1020.style.display = "block";
                } else if (node140.style.display == 'none') {
                    node140.style.display = "block";
                    oper_node1030.style.display = "block";
                }
            });
            /*  Dele Custom Button */
            $('#delete-custom').on('click', function() {
                let node110 = document.getElementById("110");
                let node120 = document.getElementById("120");
                let node130 = document.getElementById("130");
                let node140 = document.getElementById("140");
                let oper_node10 = document.getElementById("1010");
                let oper_node1010 = document.getElementById("10100");
                let oper_node1020 = document.getElementById("10200");
                let oper_node1030 = document.getElementById("10300");
                if (node140.style.display == 'block') {
                    node140.style.display = "none";
                    oper_node1030.style.display = "none";
                } else if (node130.style.display == 'block') {
                    node130.style.display = "none";
                    oper_node1020.style.display = "none";
                } else if (node120.style.display == 'block') {
                    node120.style.display = "none";
                    oper_node1010.style.display = "none";
                } else if (node110.style.display == 'block') {
                    node110.style.display = "none";
                    oper_node10.style.display = "none";
                }
            });
            /*  Reload pae */
            $('#reload-page').on('click', function() {
                window.location.reload();
            });


            /** Process Properties */
            $('#procesar-properties').click(function() {
                var json = document.getElementById('main-filter');
                var str = json.getElementsByTagName('li');
                var arr = [].slice.call(str);
                var mainJsonObject = new Object();
                var jsonArray = new Array();
                var nodeNested = 0;
                var nodeNested2 = 0;
                var nodeNested3 = 0;
                mainJsonObject.nombre = document.getElementById("tittle-filter").innerText;
                mainJsonObject.origen = document.getElementById("tool-origin").innerText;
                mainJsonObject.destino = document.getElementById("tool-target").innerText;
                arr.forEach(function(element, index, array) {
                    if (element.getAttribute('style') == "display: block;") {
                        if (element.getElementsByTagName('ol').length > 0) {
                            if (index >= nodeNested) {
                                let nestedArray = new Array();
                                numNested = element.lastElementChild.getElementsByTagName('li').length;
                                var nestedValue = element.lastElementChild.getElementsByTagName('li');
                                var arrNested = [].slice.call(nestedValue);
                                if (element.getElementsByTagName('table').length > 0) {
                                    let condition = new Object();
                                    condition.field = element.getElementsByTagName('td')[0].innerText;
                                    condition.operator = element.getElementsByTagName('td')[1].innerText;
                                    condition.value = element.getElementsByTagName('td')[2].innerText;
                                    nestedArray.push(condition);
                                } else {
                                    let operator = new Object();
                                    operator.operador = element.innerText;
                                    nestedArray.push(operator);
                                }
                                arrNested.forEach(function(elementN, index2) {
                                    if (elementN.getElementsByTagName('ol').length > 0) {
                                        let nestedArray2 = new Array();
                                        numNested2 = elementN.lastElementChild.getElementsByTagName('li').length;
                                        var nestedValue2 = elementN.lastElementChild.getElementsByTagName('li');
                                        var arrNested2 = [].slice.call(nestedValue2);
                                        if (elementN.getElementsByTagName('table').length > 0) {
                                            let condition = new Object();
                                            condition.field = elementN.getElementsByTagName('td')[0].innerText;
                                            condition.operator = elementN.getElementsByTagName('td')[1].innerText;
                                            condition.value = elementN.getElementsByTagName('td')[2].innerText;
                                            nestedArray2.push(condition);
                                        } else {
                                            let operator = new Object();
                                            operator.operador = elementN.innerText;
                                            nestedArray2.push(operator);
                                        }
                                        arrNested2.forEach(function(elementN2, index3) {
                                            if (elementN2.getElementsByTagName('table').length > 0) {
                                                let condition = new Object();
                                                condition.field = elementN2.getElementsByTagName('td')[0].innerText;
                                                condition.operator = elementN2.getElementsByTagName('td')[1].innerText;
                                                condition.value = elementN2.getElementsByTagName('td')[2].innerText;
                                                nestedArray2.push(condition);
                                            } else {
                                                let operator = new Object();
                                                operator.operador = elementN2.innerText;
                                                nestedArray2.push(operator);
                                            }
                                            nodeNested3 = index3 + 1;
                                        });
                                        nestedArray.push(nestedArray2);
                                        nodeNested2 = numNested2 + nodeNested3;
                                    }
                                    if (index2 >= nodeNested2) {
                                        if (elementN.getElementsByTagName('table').length > 0) {
                                            let condition = new Object();
                                            condition.field = elementN.getElementsByTagName('td')[0].innerText;
                                            condition.operator = elementN.getElementsByTagName('td')[1].innerText;
                                            condition.value = elementN.getElementsByTagName('td')[2].innerText;
                                            nestedArray.push(condition);
                                        } else {
                                            let operator = new Object();
                                            operator.operador = elementN.innerText;
                                            nestedArray.push(operator);
                                        }
                                    }
                                });
                                jsonArray.push(nestedArray);
                                nodeNested = index + numNested + 1;
                            }
                        }
                        if (index >= nodeNested) {
                            if (element.getElementsByTagName('table').length > 0) {
                                let condition = new Object();
                                condition.field = element.getElementsByTagName('td')[0].innerText;
                                condition.operator = element.getElementsByTagName('td')[1].innerText;
                                condition.value = element.getElementsByTagName('td')[2].innerText;
                                jsonArray.push(condition);
                            } else {
                                let operator = new Object();
                                operator.operador = element.innerText;
                                jsonArray.push(operator);
                            }
                        }
                    }
                });
                mainJsonObject.filtros = jsonArray;
                var jsonOutPut = JSON.stringify(mainJsonObject, null, 6);
                if (jsonOutPut.includes('-Properties-') || jsonOutPut.includes('Empty') ||
                    jsonOutPut.includes('-Expression-') || jsonOutPut.includes('-Operators-') ||
                    jsonOutPut.includes('not selected')) {
                    alert("Es necesario ingresar todos los parámetros");
                } else {
                    document.getElementById("textOutPut").value = jsonOutPut;
                }
            });

            /** Test to Read array table */
            $('#procesar-properties1').click(function() {
                var json = document.getElementById('main-filter');
                console.log(mainJsonObject);
                document.getElementById("textOutPut").value = json.innerHTML;
            });


        });


        /** ------------------------------- Funciones js ------------------------------- */
    </script>