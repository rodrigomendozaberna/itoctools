    <!-- Nestable -->
    <script src="{{ URL::asset('theme/assets/node_modules/nestable/jquery.nestable.js') }}"></script>

    <script>
        $(function() {

            /* ------------------------------- Nestable ------------------------------- */
            var updateOutputP = function(e) {
                var list = e.length ? e : $(e.target),
                    output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            $('#nestableP').nestable({
                group: 1
            }).on('change', updateOutputP);
            $('#nestableP2').nestable({
                group: 1
            }).on('change', updateOutputP);
            updateOutputP($('#nestableP').data('output', $('#nestable-output')));
            updateOutputP($('#nestableP2').data('output', $('#nestable2-output')));

            /* ------------------------------- functions for config params ------------------------------- */
            $('#add-option').on('click', function() {
                var node = document.createElement("LI");
                node.className = "dd-item dd3-item";
                var newDiv = document.createElement("div");
                newDiv.className = "dd-handle dd3-handle";
                var newDiv2 = document.createElement("div");
                newDiv2.className = "dd3-content";
                newDiv2.innerHTML = '<div class="tags-default"> <input type="text" value="" data-role="tagsinput" placeholder="Añadir parámetros" /> </div>';
                newDiv2.appendChild(newDiv);
                node.appendChild(newDiv2);
                document.getElementById("listaEditable").appendChild(node);
            });
            $('#add-severidad').click(function() {
                var node = document.createElement("LI");
                node.className = "dd-item dd3-item";
                var newDiv = document.createElement("div");
                newDiv.className = "dd-handle dd3-handle";
                var newDiv2 = document.createElement("div");
                newDiv2.className = "dd3-content";
                newDiv2.innerHTML = '<select class="select2 form-control custom-select" style="width: 100%; height:36px;"> <option>Severity</option> <option value="AK">Crtitical</option> <option value="HI">Normal</option> <option value="HI">Low</option> </select>';
                newDiv2.appendChild(newDiv);
                node.appendChild(newDiv2);
                document.getElementById("listaEditable").appendChild(node);
            });
            /** Consultar a Base de Datos */
            $('#Consult-database').click(function() {
                var origin = document.getElementById('selectOrigin');
                document.getElementById("textArea").style.display = "none";
                document.getElementById("main-ol").style.display = "block";
                document.getElementById("get-json").style.display = "none";
                document.getElementById("main-ol-created").remove();
            });
            /** Insertar json */
            $('#Insert-json').click(function() {
                var origin = document.getElementById('selectOrigin');
                document.getElementById("textArea").style.display = "block";
                document.getElementById("json-text").value = "";
                document.getElementById("main-ol").style.display = "none";
                document.getElementById("get-json").style.display = "block";
                document.getElementById("main-ol-created").remove();
            });
            /** Leer y dibujar json */
            $('#get-json').click(function() {
                loadJson();
            });
            /** Reecargar json cuando se mueve un valor 
            $('#nestableP').on('change', function() {
                var jsonTxt = document.getElementById('json-text').value;
                if (jsonTxt != "") {
                    document.getElementById("main-ol-created").remove();
                    loadJson();
                }
            });*/
            /** Read json */
            $('#read-json').click(function() {
                var json = document.getElementById('json-result');
                const str = json.getElementsByTagName('li')[0].innerHTML;
                var stringByLines = str;
                let lines = [];
                let initial = 0;
                let lenghtStr = str.length;
                while (initial <= lenghtStr) {
                    if (stringByLines.search('\n') != -1) {
                        var indexNumber = stringByLines.search('\n');
                        let line = (stringByLines.substring(0, indexNumber)).trim();
                        initial = indexNumber + 1;
                        stringByLines = (stringByLines.substring(initial, lenghtStr)).trim();
                        lines.push(line);
                    } else {
                        initial = lenghtStr + 1;
                    }
                }
                console.log(lines);
                document.getElementById("nestable-menu").appendChild(json);
            });
        });

        /** ------------------------------- Funciones js ------------------------------- */

        function createJsonOutPut(lines) {
            jsonMap = new Map();
            lines.forEach(function(element, index, array) {
                if (element.includes('dd-handle')) {
                    let initial = 0;
                    while (initial <= element.length) {
                        if (element.includes('dd-handle')) {
                            let init = element.indexOf('dd-handle\">') + 11;
                            let ends = element.indexOf('</div>') - 1;
                            let key = element.substring(init, ends);
                            if (element.includes('dd3-content', ends)) {
                                let initValue = element.indexOf('dd3-content\">', ends) + 13;
                                let endsValue = element.indexOf('</div>', ends) - 1;
                                let value = element.substring(initValue, endsValue);
                                initial = endsValue;
                                jsonMap.set(key, value);
                            } else {
                                initial = element.length;
                            }
                        }
                        initial = element.length;
                    }
                }
                if (element.includes('ol')) {
                    lines[index].forEach(function(element1, index1, array1) {
                        var child = new Map();
                        if (element1.includes('dd-handle')) {
                            let init1 = element1.indexOf('>') + 1;
                            let ends1 = element1.indexOf('<') - 1;
                            let key1 = element1.substring(init1, ends1);
                            child.push(key1);
                            if (element.includes('ol')) {
                                lines[index1].forEach(function(element2, index2, array2) {
                                    var child2 = [];
                                    if (element2.includes('dd-handle')) {
                                        let init2 = element2.indexOf('>') + 1;
                                        let ends2 = element2.indexOf('<') - 1;
                                        let key2 = element2.substring(init2, ends2);
                                        child2.push(key2);
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

        function loadJson() {
            var jsonTxt = document.getElementById('json-text').value;
            const obj = JSON.parse(jsonTxt);
            console.log(obj);
            var mainOl = document.createElement("ol");
            mainOl.className = "dd-list";
            mainOl.id = "main-ol-created"
            Object.entries(obj).forEach(([key, value]) => {
                console.log(key + ' : ' + value);
                var mainLi = document.createElement("li");
                mainLi.className = "dd-item";
                var newDiv = document.createElement("div");
                newDiv.className = "dd-handle";
                newDiv.innerHTML = key;
                mainLi.appendChild(newDiv);
                if (typeof value != "object") {
                    var olNodeValue = document.createElement("ol");
                    olNodeValue.className = "dd-list";
                    var valueLi = document.createElement("li");
                    valueLi.className = "dd-item";
                    var divValue = document.createElement("div");
                    divValue.className = "dd3-content";
                    divValue.innerHTML = value;
                    valueLi.appendChild(divValue);
                    olNodeValue.appendChild(valueLi);
                    mainLi.appendChild(olNodeValue);
                }
                if (typeof value == "object") {
                    var olNode = document.createElement("ol");
                    olNode.className = "dd-list";
                    Object.entries(value).forEach(([keyChild, valueChild]) => {
                        console.log('----' + keyChild + ' : ' + valueChild);
                        var liNode = document.createElement("li");
                        liNode.className = "dd-item";
                        var divChild = document.createElement("div");
                        divChild.className = "dd-handle";
                        divChild.innerHTML = keyChild;
                        liNode.appendChild(divChild);
                        if (typeof valueChild != "object") {
                            var olNodeValueC = document.createElement("ol");
                            olNodeValueC.className = "dd-list";
                            var valueLiC = document.createElement("li");
                            valueLiC.className = "dd-item";
                            var divValueC = document.createElement("div");
                            divValueC.className = "dd3-content";
                            divValueC.innerHTML = valueChild;
                            valueLiC.appendChild(divValueC);
                            olNodeValueC.appendChild(valueLiC);
                            liNode.appendChild(olNodeValueC);
                        }
                        if (typeof valueChild == "object") {
                            var olNode1 = document.createElement("ol");
                            olNode1.className = "dd-list";
                            Object.entries(valueChild).forEach(([keyChild1, valueChild1]) => {
                                console.log('++++' + keyChild1 + ' : ' + valueChild1);
                                var liNode1 = document.createElement("li");
                                liNode1.className = "dd-item";
                                var divChild1 = document.createElement("div");
                                divChild1.className = "dd-handle";
                                divChild1.innerHTML = keyChild1;
                                liNode1.appendChild(divChild1);
                                if (typeof valueChild1 != "object") {
                                    var olNodeValueC1 = document.createElement("ol");
                                    olNodeValueC1.className = "dd-list";
                                    var valueLiC1 = document.createElement("li");
                                    valueLiC1.className = "dd-item";
                                    var divValueC1 = document.createElement("div");
                                    divValueC1.className = "dd3-content";
                                    divValueC1.innerHTML = valueChild1;
                                    valueLiC1.appendChild(divValueC1);
                                    olNodeValueC1.appendChild(valueLiC1);
                                    liNode1.appendChild(olNodeValueC1);
                                }
                                if (typeof valueChild1 == "object") {
                                    var olNode2 = document.createElement("ol");
                                    olNode2.className = "dd-list";
                                    Object.entries(valueChild1).forEach(([keyChild2, valueChild2]) => {
                                        console.log('++++' + keyChild2 + ' : ' + valueChild2);
                                        var liNode2 = document.createElement("li");
                                        liNode2.className = "dd-item";
                                        var divChild2 = document.createElement("div");
                                        divChild2.className = "dd-handle";
                                        divChild2.innerHTML = keyChild2;
                                        liNode2.appendChild(divChild2);
                                        if (typeof valueChild2 != "object") {
                                            var olNodeValueC2 = document.createElement("ol");
                                            olNodeValueC2.className = "dd-list";
                                            var valueLiC2 = document.createElement("li");
                                            valueLiC2.className = "dd-item";
                                            var divValueC2 = document.createElement("div");
                                            divValueC2.className = "dd3-content";
                                            divValueC2.innerHTML = valueChild2;
                                            valueLiC2.appendChild(divValueC2);
                                            olNodeValueC2.appendChild(valueLiC2);
                                            liNode2.appendChild(olNodeValueC2);
                                        }
                                        olNode2.appendChild(liNode2);
                                    });
                                    liNode1.appendChild(olNode2);
                                }
                                olNode1.appendChild(liNode1);
                            });
                            liNode.appendChild(olNode1);
                        }
                        olNode.appendChild(liNode);
                    });
                    mainLi.appendChild(olNode);
                }
                mainOl.appendChild(mainLi);
            });
            document.getElementById("nestableP").appendChild(mainOl);
            document.getElementById("textArea").style.display = "none";
            document.getElementById("main-ol").style.display = "none";
            document.getElementById("get-json").style.display = "none";
        }
    </script>