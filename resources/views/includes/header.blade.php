        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" >
                    <a class="navbar-brand" href="javascript:void(0)">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{ URL::asset('theme/assets/images/logo-icon.png') }}" alt="homepage"
                                class="dark-logo" width="50px" />
                            <!-- Light Logo icon -->
                            <img src="{{ URL::asset('theme/assets/images/logo-light-icon.png') }}" alt="homepage"
                                class="light-logo" width="50px" />
                        </b>
                        <!--End Logo icon -->

                        <!-- Logo text -->
                        <span>
                            <!-- dark Logo text -->
                            <img src="{{ URL::asset('theme/assets/images/logo-text.png') }}" alt="homepage"
                                class="dark-logo" width="70px" />
                            <!-- Light Logo text -->
                            <img src="{{ URL::asset('theme/assets/images/logo-light-text.png') }}" class="light-logo"
                                alt="homepage" width="70px" /></span>
                        <!--End Logo text -->
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul id="navitem" class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a
                                class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark"
                                href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item">
                            <form class="app-search d-none d-md-block d-lg-block">
                                <input type="text" class="form-control" placeholder="Search & enter">
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                      
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                    
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <!-- <li class="nav-item dropdown">
                            @php
                                $locale = session()->get('locale');
                                if($locale) {
                                    if($locale == 'es'){
                                        $locale = 'mx';
                                    }
                                }
                                else{
                                    $locale = 'mx';
                                }
                            @endphp
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon {{ 'flag-icon-'.$locale }}"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated bounceInDown">
                                <a class="dropdown-item" href="{{ URL::asset('/lang/es') }}"><i
                                        class="flag-icon flag-icon-mx"></i>
                                    {{ __('Spanish') }}
                                </a>
                                <a class="dropdown-item" href="{{ URL::asset('/lang/us') }}"><i
                                        class="flag-icon flag-icon-us"></i>
                                    {{ __('English') }}
                                </a>
                            </div>
                        </li> -->
                        <!-- ============================================================== -->
                        <!-- End Language -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- mega menu -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- End mega menu -->
                        <!-- ============================================================== -->
                        <li class="nav-item right-side-toggle">
                            <a class="nav-link  waves-effect waves-light" href="javascript:void(0)">
                                <i class="fas fa-angle-left fa-lg"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>