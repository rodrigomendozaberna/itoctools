<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div>
                    <img src="{{ URL::asset('theme/assets/images/users/admin.png') }}" alt="user-img" class="img-circle">
                </div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="caret"></span></a>
                    <div class="dropdown-menu animated fadeIn">
                        <!-- text-->
                        <a href="{{ URL::to('/user/profile') }}" class="dropdown-item"><i class="ti-user"></i> Mi perfil</a>
                        <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Configuración</a>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Salir</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <!-- text-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!--<li class="nav-small-cap">--- Zenoss</li>-->
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-cog"></i>
                        <span class="hide-menu"> Administración</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-user-shield"></i>
                                <span class="ml-1">Accesos</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ URL::to('admin/users') }}">
                                        <i class="fas fa-users"></i>
                                        <span class="ml-1">Usuarios</span>
                                    </a>
                                </li>
                                <li><a href="{{ URL::to('admin/roles') }}">
                                        <i class="fas fa-shapes ml-1"></i>
                                        <span class="ml-1">Roles</span>
                                    </a>
                                </li>
                                <li><a href="{{ URL::to('admin/abilities') }}">
                                        <i class="fas fa-check-double ml-1"></i>
                                        <span class="ml-1">Permisos</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!--  ------------------------- Eventos -->
                <li>
                    <a class="waves-effect waves-dark" href="{{ URL::to('/events') }}" aria-expanded="false">
                        <i class="fab fa-firefox"></i>
                        <span class="hide-menu"> Eventos
                        </span>
                    </a>
                </li>
                <!--  ------------------------- Configuración de Filtros -->
                <li>
                    <a class="waves-effect waves-dark" href="{{ URL::to('/filters') }}" aria-expanded="false">
                        <i class="ti-filter"></i>
                        <span class="hide-menu"> Configuración de Filtros
                        </span>
                    </a>
                </li>
                <!--  ------------------------- Configuración de json drag and drop -->
                <li>
                    <a class="waves-effect waves-dark" href="{{ URL::to('/configParam') }}" aria-expanded="false">
                        <i class="far fa-edit"></i>
                        <span class="hide-menu"> Configuración de parámetros
                        </span>
                    </a>
                </li>
                <!--<li class="nav-small-cap">--- OTROS</li>-->
                <li>
                    <a class="waves-effect waves-dark" aria-expanded="false" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off text-danger"></i><span class="hide-menu"> Salir </span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>