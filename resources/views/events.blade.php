@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Eventos</h4>
                <div class="row">
                    <div class="col">
                        <table class="table color-table  table-responsive table-striped table-hover w-100 d-block d-md-table table-md" id="events-table">
                            <thead>
                                <tr>
                                    <th>Aplicación</th>
                                    <th>Fecha de Recepción</th>
                                    <th>Categoría</th>
                                    <th>Severidad</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($events as $item)
                                <tr>
                                    <th>{{$item['application']}}</th>
                                    <th>{{$item['time_received_label']}}</th>
                                    <th>{{$item['category']}}</th>
                                    <th>{{$item['severity']}}</th>
                                    <th>{{$item['state']}}</th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection