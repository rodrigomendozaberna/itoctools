@extends('layouts.master')

@section('content')

<div class="row" id="nestable-param">

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body" id="main-card">
                <h4 class="card-title">Json de Entrada</h4>
                <div class="col-sm-4">
                    <select class="selectpicker m-b-20 m-r-10" data-style="btn-primary" id="selectOrigin">
                        <option value="New Relic" data-tokens="New Relic">New Relic</option>
                        <option value="Youbora" data-tokens="Youbora">Youbora</option>
                        <option value="Zenoss" data-tokens="Zenoss">Zenoss</option>
                    </select>
                </div>
                <div class="form-group" id="textArea" style="display: none;">
                    <label>Text area</label>
                    <textarea class="form-control" rows="5" id="json-text"></textarea>
                </div>
                <div class="myadmin-dd-empty dd" id="nestableP">
                    <ol class="dd-list" id="main-ol" style="display: none;">
                        <li class="dd-item" data-id="1">
                            <div class="dd-handle"> AlarmDescription</div>
                        </li>
                        <li class="dd-item" data-id="2">
                            <div class="dd-handle"> AlarmName </div>
                            <ol class="dd-list">
                                <li class="dd-item" data-id="3">
                                    <div class="dd-handle"> MessageId </div>
                                </li>
                                <li class="dd-item" data-id="4">
                                    <div class="dd-handle"> Subject </div>
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle"> Timestamp </div>
                                        </li>
                                    </ol>
                                </li>
                                <li class="dd-item" data-id="6">
                                    <div class="dd-handle"> Fecha </div>
                                </li>
                            </ol>
                        </li>
                        <li class="dd-item" data-id="7">
                            <div class="dd-handle"> Dimensions</div>
                        </li>
                        <li class="dd-item" data-id="8">
                            <div class="dd-handle"> StateChangeTime </div>
                            <ol class="dd-list">
                                <li class="dd-item" data-id="9">
                                    <div class="dd-handle"> AlarmName </div>
                                </li>
                            </ol>
                        </li>
                    </ol>
                </div>
                <div class="button-box m-t-20">
                    <a id="get-json" class="btn btn-danger" href="javascript:void(0)" style="display: none;">Procesar json</a>
                    <a id="Consult-database" class="btn btn-success" href="javascript:void(0)">Consultar BD</a>
                    <a id="Insert-json" class="btn btn-info" href="javascript:void(0)">Insertar json</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Campos de Salida</h4>
                <div class="col-sm-4">
                    <select class="selectpicker m-b-20 m-r-10" data-style="btn-primary">
                        <option data-tokens="OMI">OMI</option>
                        <option data-tokens="Plantilla HTML">Plantilla HTML</option>
                        <option data-tokens="Correo">Correo</option>
                    </select>
                </div>
                <div class="myadmin-dd-empty dd" id="nestableP2">
                    <ol class="dd-list" id="json-result">
                        <li class="dd-item" data-id="10">
                            <div class="dd-handle"> Formato OMI </div>
                            <ol class="dd-list" id="listaEditable">
                                <li class="dd-item" data-id="11">
                                    <div class="dd-handle"> custom </div>
                                </li>
                                <li class="dd-item" data-id="12">
                                    <div class="dd-handle"> fijos </div>
                                </li>
                            </ol>
                        </li>
                    </ol>
                    <div class="button-box m-t-20">
                        <a id="read-json" class="btn btn-danger" href="javascript:void(0)">Read Json</a>
                        <a id="add-severidad" class="btn btn-info" href="javascript:void(0)">Severidad</a>
                        <a id="add-option" class="btn btn-success" href="javascript:void(0)">Agregar parámetro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Evanto de Salida</h4>
                <div class="myadmin-dd-empty dd" id="nestable-menu">
                    <ol class="dd-list">
                        <li class="dd-item" data-id="50">
                        </li>
                    </ol>
                </div>
                <div class="button-box m-t-20">
                    <a id="add-save" class="btn btn-danger" href="javascript:void(0)">Guardar formato</a>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('allJquery')
@include('includes.methodsConfigP')
@endsection