@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 text-right">
                        @if ((Auth::user()->can('users_manage')))
                        <a href="{{ route('roles.edit', $role->id) }}">
                            <button type="button" class="btn btn-circle btn-primary" onclick="javascript();">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                        </a>
                        @endif
                    </div>
                </div>
                <h4 class="card-title">DETALLE DEL ROL {{ $role->id }} </h4>
                <h6 class="card-subtitle"> Detalle del rol.</h6>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Nombre</label>
                    <label class="col-10 col-form-label">{{ $role->name }}</label>
                </div>
                <div class="form-group m-t-10 row">
                    <label class="col-2 col-form-label text-right">Descripción</label>
                    <label class="col-10 col-form-label">{{ $role->title }}</label>
                </div>
                <div class="form-group m-t-40 row">
                    <label class="col-2 col-form-label text-right">Habilidades</label>
                    <div class="col-10">
                        @foreach ($role->abilities()->pluck('name') as $ability)
                            <span class="label label-info label-many">{{ $ability }}</span>
                        @endforeach
                    </div>
                </div>
                <br>
                <div class="row text-right">
                    <div class="col">
                        <a href="{{ route('roles.index') }}" class="btn btn-warning">Cancelar </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection