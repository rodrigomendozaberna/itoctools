@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justifycontent">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Tools</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div >
                        <table class="table color-table muted-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Creado</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tools as $tool)
                                <tr>
                                    <td>{{ $tool->id }}</td>
                                    <td>{{ $tool->tool }}</td>
                                    <td>{{ $tool->description }}</td>
                                    <td>{{ $tool->created_at }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opciones
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $tools->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection