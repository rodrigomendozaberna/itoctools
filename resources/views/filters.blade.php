@extends('layouts.master')

@section('content')
<div class="row" id="nestable-filter">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body" id="main-card">
                <h3 class="card-title">Configuración de filtros</h3>
                <table style="clear: both" class="table table-bordered table-striped" id="user">
                    <tbody>
                        <tr>
                            <td width="50%">Nombre del filtro:</td>
                            <td width="50%">
                                <a href="javascript:void(0)" id="tittle-filter" data-type="text" data-pk="1" data-placement="right" data-placeholder="Campo requerido"></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Origen:</td>
                            <td>
                                <a href="javascript:void(0)" id="tool-origin" data-type="select" data-pk="1" data-value=""></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Destino:</td>
                            <td>
                                <a href="javascript:void(0)" id="tool-target" data-type="select" data-pk="1" data-value=""></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="myadmin-dd-empty dd" id="nestable">
                    <ol class="dd-list" id="main-filter">
                        <li class="dd-item dd3-item" data-id="1" id="1" style="display: block;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="11" id="11" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="2" id="2" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties2" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression2" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter2" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="22" id="22" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators2" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="3" id="3" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties3" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression3" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter3" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="33" id="33" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators3" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="4" id="4" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties4" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression4" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter4" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="44" id="44" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators4" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="5" id="5" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties5" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression5" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter5" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="55" id="55" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators5" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="6" id="6" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties6" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression6" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter6" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="66" id="66" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators6" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="7" id="7" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties7" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression7" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter7" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="77" id="77" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators7" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="8" id="8" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties8" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression8" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter8" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="88" id="88" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators8" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="9" id="9" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties9" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression9" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter9" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="99" id="99" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators9" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="10" id="10" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="properties10" data-type="select" data-pk="1" data-value="" data-title="Select propertie"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression10" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter10" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="1010" id="1010" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators10" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="110" id="110" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1101" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression11" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1102" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="10100" id="10100" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators11" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="120" id="120" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1201" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression12" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1202" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="10200" id="10200" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators12" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="130" id="130" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1301" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression13" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1302" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="10300" id="10300" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <td width="100%"><a href="javascript:void(0)" id="operators13" data-type="select" data-pk="1" data-value="" data-title="Select Operators"></a></td>
                            </div>
                        </li>
                        <li class="dd-item dd3-item" data-id="140" id="140" style="display: none;">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                                <table width="100%" id="user">
                                    <tbody>
                                        <tr>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1401" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                            <td width="30%"><a href="javascript:void(0)" id="expression14" data-type="select" data-pk="1" data-value="" data-title="Select operator"></a></td>
                                            <td width="35%"><a href="javascript:void(0)" id="inline-parameter1402" data-type="text" data-pk="1" data-title="Enter Parameter"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ol>
                </div>
                <div class="button-box m-t-20">
                    <a id="add-row" class="btn btn-info" href="javascript:void(0)">+ Agregar Condición </a>
                    <a id="delete-row" class="btn btn-danger" href="javascript:void(0)">- Eliminar Condición</a>
                    <a id="procesar-properties" class="btn btn-success" href="javascript:void(0)">OK</a>
                </div>
                <div class="button-box m-t-20">
                    <a id="add-custom" class="btn btn-info" href="javascript:void(0)">+ Condición Custom</a>
                    <a id="delete-custom" class="btn btn-danger" href="javascript:void(0)">- Condición Custom</a>
                    <a id="reload-page" class="btn btn-warning" href="javascript:void(0)">
                        <i class="icon-refresh"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Json del Filtro</h4>
                <div id="nestable-menu">
                    <textarea id="textOutPut" rows="27" cols="60" disabled></textarea>
                </div>
                <div class="button-box m-t-20">
                    <a id="add-save" class="btn btn-danger" href="javascript:void(0)">Guardar formato</a>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('allJquery')
@include('includes.methods_filtersConfig')
@endsection