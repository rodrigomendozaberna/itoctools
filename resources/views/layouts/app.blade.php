<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/style.min.css') }}">
    <!-- Login -->
    <link rel="stylesheet" href="{{ URL::asset('theme/ecommerce/dist/css/pages/login-register-lock.css') }}">
    <!-- Bootstrap tether core JavaScript -->
    <!-- Lines commented to avoid issues and warnings -->
    <script src="{{ URL::asset('theme/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ URL::asset('theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('theme/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>


    <!-- Custom JavaScript -->
    <script src="{{ URL::asset('theme/ecommerce/dist/js/custom.js') }}"></script>
    
</head>

<body>
    <main>
        @yield('content')
    </main>
</body>

</html>
