<?php

namespace App\Http\Controllers;

use App\Filters;
use Illuminate\Http\Request;

class FiltersController extends Controller
{
    /**
     * Vista de configuración de filtros
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('filters')->with([
            'title' => 'Configuración de Filtros',
            'breadcrumb' => [
                [
                    'title' => 'Configuración de Filtros'
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filters = Filters::create([
            'name' => $request->name,
            'json' => json_encode($request->json),
        ]);

        return response()->json('Add Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function show(Filters $filters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function edit(Filters $filters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Filters $filters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function destroy(Filters $filters)
    {
        //
    }
}
