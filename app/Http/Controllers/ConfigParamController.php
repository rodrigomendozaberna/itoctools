<?php

namespace App\Http\Controllers;

class ConfigParamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('config_param')->with([
            'title' => 'Configuración de Parámetros',
            'breadcrumb' => [
                [
                    'title' => 'Configuración de Parámetros'
                ]
            ]
        ]);
    }

    
}
