<?php

namespace App\Http\Controllers\Admin;

use Silber\Bouncer\Database\Ability;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class AbilityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        //$this->validaRoleUser('users_manage');
        $abilities = Ability::all();
        return datatables()->of($abilities)->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Permisos';
        $breadcrumb = [
            [
                'title' => 'Permisos',
            ]
        ];
        return view('admin.abilities.ability')->with(compact('title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$this->validaRoleUser('create');
        $title = 'Permisos';
        $breadcrumb = [
            [
                'title' => 'Permisos',
                'url' => route('abilities.index'),
            ],
            [
                'title' => 'Nuevo',
            ],
        ];

        return view('admin.abilities.create')->with(compact('title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validaRoleUser('users_manage');
        // Validar datos
        $messages = [];
        $rules = [
            'name' => ['required', 'min:3', 'max:20', 'unique:abilities'],
            'title' => ['max:50']
        ];
        $this->validate($request, $rules, $messages);

        Ability::create($request->all());

        $request->session()->flash(
            'notification',
            [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'La habilidad se guardo correctamente',
            ]
        );

        return redirect()->route('abilities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function show(Ability $ability)
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Detalle';
        $breadcrumb = [
            [
                'title' => 'Detalle',
                'url' => route('abilities.index'),
            ],
            [
                'title' => 'Detalle',
            ],
        ];

        return view('admin.abilities.show')->with(compact('ability', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function edit(Ability $ability)
    {
        //$this->validaRoleUser('users_manage');
        $title = 'Permisos';
        $breadcrumb = [
            [
                'title' => 'Permisos',
                'url' => route('abilities.index'),
            ],
            [
                'title' => 'Editar',
            ],
        ];

        return view('admin.abilities.edit')->with(compact('ability', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ability $ability)
    {
        //$this->validaRoleUser('users_manage');
        // Validar datos
        $messages = [];
        $rules = [
            // Case sensitive = strtolower
            'name' => ['required', 'min:3', ($ability->name != $request->name ? 'unique:abilities' : '')]
        ];
        $this->validate($request, $rules, $messages);

        $ability->update($request->all());

        $request->session()->flash(
            'notification',
            [
                'type' => 'success',
                'header' => '¡Bien hecho!',
                'message' => 'El permiso se actualizó correctamente',
            ]
        );

        return redirect()->route('abilities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ability  $ability
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ability $ability)
    {
        //$this->validaRoleUser('users_manage');
        $ability->delete();
        return redirect()->route('admin.abilities.index');
    }

    /**
     * Delete all selected Ability at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        //$this->validaRoleUser('users_manage');
        if ($request->input('ids')) {
            $entries = Ability::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    /**
     * Valida si el usuario cuenta con la habilidad necesaria.
     *
     * @param String $ability
     */
    public function validaRoleUser($ability)
    {
        if (!Gate::allows($ability)) {
            return abort(401);
        }
    }
}
