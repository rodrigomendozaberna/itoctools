<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

// --- habilidades, roles, usuarios
Route::get('users-list', 'Admin\UserController@getAll');
Route::get('roles-list', 'Admin\RoleController@getAll');
Route::get('abilities-list', 'Admin\AbilityController@getAll');

// --- Se crea un prefijo para eliminar el /admin/ de todas las url ---//
Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::resource('/users', 'Admin\UserController');
    Route::resource('/roles', 'Admin\RoleController');
    Route::resource('/abilities', 'Admin\AbilityController');
});

// --- Se agrupan para aplicar el middleware de auth ---//
Route::middleware(['auth'])->group(function () {
    Route::get('tools', 'ToolController@index')->name('tools');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/user/profile', 'ProfileController@breadcrumb')->name('profile');
    Route::resource('configParam', 'ConfigParamController');
    Route::resource('filters', 'FiltersController');
    Route::resource('events', 'EventsController');
    Route::resource('event', 'EventsController');

});

// --- Cambiar el lenguaje ---//
Route::get('lang/{locale}', 'LangController@index');
